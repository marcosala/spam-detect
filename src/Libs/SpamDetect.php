<?php
namespace Libs;

class SpamDetect {
  
    private $message;
    private $email;
    private $pageLoadTS;
    private $positions = ['left','middle','right'];

    private $blacklistMails = [];
    private $blacklistMessages = [];
    private $matchFunctions = [];
    private $matchMail = [];
    private $matchMessage = [];

    private static $rules = [
        [
          'function' => 'checkHtml',
          'description' => 'Verifica la presenza di codice HTML nel testo del messaggio',
          'active' => 1,
          'auto' => 1
        ],
        [
          'function' => 'checkTimestamp',
          'description' => 'Controlla se il tempo di invio della richiesta è inferiore ai 3 secondi',
          'active' => 1,
          'auto' => 1 
        ],
        [
          'function' => 'checkNonLatinCharacters',
          'description' => 'Verifica se il messaggio contiente caratteri non europei',
          'active' => 1, 
          'auto' => 1
        ],
        [
          'function' => 'checkMessagesRules',
          'description' => 'Verifica se il messaggio contiene una regola definita nella blacklist dei messaggi',
          'active' => 1,
          'auto' => 0
        ],
        [
          'function' => 'checkEmailRules',
          'description' => 'Verifica se la mail contiente una regola definita nella blacklist delle email',
          'active' => 1,
          'auto' => 0
        ]  
    ];


    public function addMessageRule($part,$position){
        if(!in_array($position, $this->positions))
            return false;

        $this->blacklistMessages[] = [
            "part" => $part,
            "position" => $position  
        ];  
    }

    public function addMailRule($part,$position){
        if(!in_array($position, $this->positions))
            return false;

        $this->blacklistMails[] = [
            "part" => $part,
            "position" => $position  
        ];  
    }

    function __construct($email,$message,$pageLoadTS) {
        $this->setEmail($email);
        $this->setMessage($message);
        $this->setPageLoadTS($pageLoadTS);
    }

    public static function getRules(){
        return self::$rules;
    }

    public function setMessage($message){
        $this->message = $message;  
    }

    public function setEmail($email){
        $this->email = $email;  
    }

    public function setPageLoadTS($pageLoadTS){
        $this->pageLoadTS = $pageLoadTS;  
    }

    //verifica la presenza di link html all'interno del messaggio
    public function checkHtml(){
        $string = $this->message;

        if(preg_match( "/\/[a-z]*>/i", $string ) != 0) {
          return true;
        }

        return false;
    }

    public function checkNonLatinCharacters(){
        $string = $this->message;
        return boolval(preg_match('/[^\\p{Common}\\p{Latin}]/u', $string));

    }

    public function checkTimestamp(){
      
        if ((time() - $this->pageLoadTS) < 3)
            return true;

        return false;  
          
    }

    public function checkMessagesRules(){
        $spam = false;
        foreach($this->blacklistMessages as $rule){
          $string = $rule['part'];
          switch($rule['position']){
              case 'left':{
                if(substr($this->message, 0, strlen($string)) === $string){
                  $spam = true;
                  $this->matchMessage[] = $string;
                }
                break;
              }

              case 'middle':{
                if(strpos($this->message, $string)  !== false){
                  $spam = true;
                  $this->matchMessage[] = $string;
                }
                break;
              }

              case 'right':{
                if(substr($this->message, strlen($this->message) - strlen($string),strlen($this->message)) === $string){
                  $spam = true;
                  $this->matchMessage[] = $string;
                }
                break;
              }
          }
        }
        return $spam;
    }

    public function checkEmailRules(){
        $spam = false;
        foreach($this->blacklistMails as $rule){
          $string = $rule['part'];
          switch($rule['position']){
              case 'left':{
                if(substr($this->email, 0, strlen($string)) === $string){
                  $spam=true;
                  $this->matchMail[] = $string;
                }
                break;
              }

              case 'middle':{
                if(strpos($this->email, $string)  !== false){
                  $spam = true;
                  $this->matchMail[] = $string;
                }
                break;
              }

              case 'right':{
                if(substr($this->email, strlen($this->email) - strlen($string),strlen($this->email)) === $string){
                  $spam = true;
                  $this->matchMail[] = $string;
                }
                break;
              }
          }
        }
        return $spam;
    }

    
    public function getSpamInfo(){

      $message = [];
      if(!empty($this->matchFunctions)){
          $list = [];
          foreach($this->matchFunctions as $function){
              $list[] = $function;
          }

          $message[] = [
            'title' => 'Regole',
            'list' => $list
          ];
      }

      if(!empty($this->matchMessage)){
 
          $list = [];
          foreach($this->matchMessage as $msg){
              $list[] = $msg;
          }

          $message[] = [
            'title' => 'Testi',
            'list' => $list
          ];
      }

      if(!empty($this->matchMail)){
          
          $list = [];
          foreach($this->matchMail as $msg){
              $list[] = $msg;
          }

          $message[] = [
            'title' => 'Mail',
            'list' => $list
          ];
      }


      return $message;
    }
    

    public function isSPAM(){
        $spam = false;

        foreach(self::$rules as $rule){
            $func = $rule['function'];
            if($this->$func()){
              $spam = true;
              if(!in_array($rule['function'], $this->matchFunctions))
                $this->matchFunctions[] = $rule['function'];
            }

        }
       
       return $spam;    
    }

}
